/**
 * cookies 和 localstorage一样都是本地缓存
 * 区别：
 * 1、cookie  只有4kb， localstorage有5M
 * 2、生命周期不一样，localstorage只要用户不删除一直都存在，cookie可以设置过期时间，如果没有设置过期时间关闭浏览器就删除了
 * 3、cookie中的数据，会自动随着请求传递给服务端，localstorage是不会的
 * 4、语法不一样，localstorage语法比较方便简洁，cookie语法使用不方便，通过document.cookie来获取和设置
 */

import Cookies from 'js-cookie'
// 因为原生的cookie操作不方便，因此项目中下载了js-cookie插件来操作cookie
// 主要是提供增删改查cookie的方法，如下：

const TokenKey = 'hr_token-78'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
