module.exports = {

  title: '人力资源管理平台', // 设置项目名称

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false, // 设置头部是否使用固定定位

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true // 设置侧边栏导航是否显示logo
}
