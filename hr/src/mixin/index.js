import store from '@/store'
// 声明混入对象并导出
export default {
  methods: {
    // 这里面声明的函数会自动成为其他所有vue文件内
    // methods的函数(混入在其他的vue文件中成为他们的成员),
    // 其他vue文件中可以直接使用该函数
    checkPermission(point) {
      // 退出登录的时候，清空了userinfo的数据，因此这里需要做个判断，防止退出登录报错
      return store.state.user.userInfo.roles && store.state.user.userInfo.roles.points.includes(point)
    }
  }
}
