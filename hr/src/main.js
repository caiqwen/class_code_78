import Vue from 'vue'

// 重置样式 ---- 解决在不同浏览器中标签样式不同的问题
import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // 权限控制文件 --- 导航守卫

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// 我们这个项目不需要模拟数据，因此需要把这个if语句的代码注释掉
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// 为什么插件可以直接通过Vue.use方法来注册组件，是因为插件内容实现了一个install函数
// 而且，当我们用Vue.use方法来注册组件的时候，这个方法会自动调用插件内提供的install函数
// 并且会给install传递Vue，这样install函数内部就可以通过Vue.component来注册组件了
// console.log(36, ElementUI.install)
// Vue.use(ElementUI)
// 多语言切换
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})

Vue.config.productionTip = false

// 自定义全局指令，解决图片出错问题
// Vue.directive('自定义指令名',{
//   inserted(el, binding){
//     el: 调用该指令的DOM元素
//     binding.value: 使用指令时传入的值
//   }
// })
import fiximg from '@/directive/fiximg'
Vue.directive('imgerror', fiximg)

// 注册全局组件
// import PageTools from './components/PageTools/index.vue'
// Vue.component('PageTools', PageTools)

// 为什么插件可以直接通过Vue.use方法来注册组件，是因为插件内容实现了一个install函数
// 而且，当我们用Vue.use方法来注册组件的时候，这个方法会自动调用插件内提供的install函数
// 并且会给install传递Vue，这样install函数内部就可以通过Vue.component来注册组件了
import myPlugin from './components/index'
Vue.use(myPlugin)

/**
 *  全局过滤器
 */
// 语法：
// Vue.filter('过滤器的名字', (val) => {
//   表示将来使用该过滤器时传入的值
// })
// 使用方法：过滤器只能使用在插值表达式或者v-bind动态属性中
// 使用语法：变量 | 过滤器名字
// 过滤器的作用：提供一个全局方法，处理数据，并把处理后的数据返回给使用者
// 格式化日期
// import { formatDate, add } from '@/filters/index'
// const obj = {
//   formatDate, add
// }
// 导入所有的函数
// 语法：import * as 自定义名字 from '引入的文件路径'
// 表示通过*导入所有的函数，并且通过as关键词对*进行重命名
import * as obj from '@/filters/index'
// console.log(80, obj)
// Vue.filter('formatDate', formatDate)
// Vue.filter('add', add)
for (const key in obj) {
  // console.log(key)
  // 因为key是变量，因此通过变量获取属性对应的值
  // 需要用到中括号的形式来获取
  // console.log(obj[key])
  Vue.filter(key, obj[key])
}

// 混入对象的注册
import myMixin from '@/mixin'
// vue提供mixin方法专门用于注册混入对象
Vue.mixin(myMixin)

// 多语言
import i18n from '@/lang/index'

new Vue({
  el: '#app',
  i18n: i18n,
  router,
  store,
  render: h => h(App)
})
