// 封装组织架构相关的接口函数
import request from '@/utils/request'

// 获取部门列表
export function getDepartments() {
  return request({
    url: '/company/department'
  })
}

// 新增部门
export function addDepartment(data) {
  return request({
    url: '/company/department',
    data,
    method: 'post'
  })
}

// 删除部门
export function delDepartment(id) {
  return request({
    url: '/company/department/' + id,
    method: 'delete'
  })
}

// 根据ID查询部门详情
export function getDepartmentById(id) {
  return request({
    url: '/company/department/' + id
  })
}

// 根据ID修改部门详情
export function updateDepartment(data) {
  return request({
    url: '/company/department/' + data.id,
    method: 'put',
    data
  })
}
