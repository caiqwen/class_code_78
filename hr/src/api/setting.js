// 封装角色和公司信息的相关接口
import request from '@/utils/request'

// 获取角色列表
export function getRoleList(params) {
  return request({
    url: '/sys/role',
    params
  })
}

// export function getRoleList(params) {
//   return request.get('/sys/role',{params})
// }

// 添加角色
export function addRole(data) {
  return request({
    url: '/sys/role',
    method: 'post',
    data
  })
}

// 删除角色
export function delRole(id) {
  return request({
    url: '/sys/role/' + id,
    method: 'delete'
  })
}

// 根据id获取角色详情
export function getRoleById(id) {
  return request({
    url: '/sys/role/' + id
  })
}

// 根据id更新角色
export function updateRole(data) {
  return request({
    url: '/sys/role/' + data.id,
    method: 'put',
    data
  })
}

// 获取企业信息
export function getCompanyInfo(id) {
  return request({
    url: '/company/' + id
  })
}

// 给角色分配权限
export function assignPerm(data) {
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
}
