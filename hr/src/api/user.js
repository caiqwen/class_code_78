// 用于封装用户相关的接口函数的地方
import request from '@/utils/request.js'

// 登录接口
export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

// 获取用户基本资料
export function getUserInfo() {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

// 获取员工基本信息 --- 获取头像
export function getEmployeesById(id) {
  return request({
    url: '/sys/user/' + id
  })
}
