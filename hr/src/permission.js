// 配置页面访问权限的地方 --- 导航守卫
/**
 * 需求：项目是一个后台管理系统项目，因此所有的页面都必须登录后才能访问（除了登录页面-404页面之外）。
 *
 * 如何判断已登录？
 * 答：我们只需要判断token是否存在即可，存在表示已登录，不存在表示未登录
 * */

import router from '@/router/index'
import store from '@/store/index'

// 导航守卫 -- 全局前置守卫  --- 是页面跳转之前触发的函数
router.beforeEach(async(to, from, next) => {
  // to: 跳转的目标路由对象， 注意：to的值在执行导航守卫里面的代码之前已经确定了
  // from: 当前的路由对象(跳转之前)
  // next：放行函数
  if (store.state.user.token) {
    // 登录了
    // 登录成功后 不允许用户访问登录页面 -- 直接跳转到主页即可
    if (to.path === '/login') {
      next('/')
    } else {
      // 当用户信息不存在的时候才重新获取用户信息数据
      if (!store.state.user.userInfo.userId) {
        await store.dispatch('user/getUserInfo')
        // 为了保证筛选路由的时候能够获取到menus数据，
        // 因此调用actions函数的代码必须写到这个位置上。
        // 调用筛选具有访问权限路由的方法
        // 调用模块中actions函数的语法：store.dispacth('模块名/函数名', 参数))
        const newRoutes = await store.dispatch('permission/filterRoutes',
          store.state.user.userInfo.roles.menus)
        // console.log(30, newRoutes)
        // 把筛选出来的具有权限的动态路由添加到路由规则中，则可以恢复路由的访问功能了
        router.addRoutes([...newRoutes, { path: '*', redirect: '/404', hidden: true }])
        /**
         * 问题1：方访问权限路由页面的时候刷新浏览器，会跳转到404页面。
         * 原因：因为刷新浏览器的时候所有的数据(包括store)都会重置（除了token），
         * 也就是说动态路由数据已经不在了，不在的时候自然路由就会匹配到404页面
         *
         * 解决方法：把静态路由中的404页面删除掉即可，但是项目中404页面是需要的，
         * 那什么时候再重新加进去呢？
         * 答：在添加动态路由的时候一起加进去就可
         *
         * 问题2：此刻访问动态路由页面刷新浏览器的时候，会出现一片空白
         * 原因：因为刷新浏览器的时候数据都重置了，并且404页面也被我们删除了，因此匹配
         * 不到跳转的目标路由地址，找不到跳转的路由地址的情况下就会出现一片空白的情况。
         * 解决方法：此刻动态路由数据又有了，所以只需利用next重新跳转到原来访问的目标路由对象上即可
         */
        next(to.path) // 相当于重定向，重新执行导航守卫, 执行完这行代码后还是可以往下执行，但是后面next不会影响到这个跳转
      }
      next()
    }
  } else {
    // 未登录 --- 跳转到登录页面
    // 当用户访问的页面就是登录页面的时候 应该直接放行
    const whiteArr = ['/login', '/404'] // 白名单，所有在数组中的路径都可以直接放行
    // if (to.path === '/login' || to.path === '/404') {
    if (whiteArr.includes(to.path)) {
      next()
    } else {
      next('/login') // 当我们通过next跳转到某个页面的时候，会重新触发导航守卫
    }
  }
})
