// 图片异常处理
export default {
  inserted(el, binding) {
    // console.log(49, el, binding)
    // 当图片出错的时候，把图片的src设置默认图片地址
    // 如何判断图片出错了呢？
    // 答：原生js有一个事件：error事件，那么我们就可以通过error事件
    // 来监听图片是否出错
    el.addEventListener('error', function() {
      // 图片出错的时候 会触发该事件
      console.log('出错了！', binding.value)
      // 把图片路径改为传入的默认图片
      el.src = binding.value
    })
    // 判断src的值是否为null
    // 当el.src的值存在的时候，就不会执行或运算符后面的代码了
    // 当el.src的值不存在的时候 ，就会执行或运算符后面的代码
    // 并把value赋值给el.src
    el.src = el.src || binding.value
  },
  // componentUpdated 这个函数是在组件或者子组件数据更新时触发
  componentUpdated(el, binding) {
    el.addEventListener('error', function() {
      // 图片出错的时候 会触发该事件
      // 把图片路径改为传入的默认图片
      el.src = binding.value
    })
    el.src = el.src || binding.value
  }
}
