import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import permission from './modules/permission'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    // 专门用来声明全局变量的地方
    msg: '666'
  },
  mutations: {
    // 专门修改state中声明的变量的值的属性
    // mutations中声明的函数，默认能够接受到一个参数
    // 这个参数就是state，如果还想要传递一些额外的参数的话
    // 以第二参数的的形式传递即可
    setMsg(state, data) {
      state.msg = data
    }
  },
  actions: {
    // actions是vuex专门用于写异步代码的地方
    // 然后通过mutations间接修改state中的数据
    // actions中声明的函数，默认能够接收到一个
    // 参数，这个参数就是store
    asyncChangeMsg(store) {
      setTimeout(() => {
        store.commit('setMsg', 777)
      }, 1000)
    }
  },
  modules: {
    app,
    settings,
    user,
    permission
  },
  getters
})

export default store
