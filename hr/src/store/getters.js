// 应用场景： getters简化数据的读取
// getters：其实就是声明全局变量的另一种方法，
// 相当于vue的计算属性computed
const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: function(state) {
    return state.user.userInfo.staffPhoto
  },
  name: state => state.user.userInfo.username
}
export default getters
