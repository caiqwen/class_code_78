
import { asyncRoutes, constantRoutes } from '@/router'
const state = {
  routes: []
}
const mutations = {
  setRoutes(state, newRoutes) {
    state.routes = [...constantRoutes, ...newRoutes]
  }
}

const actions = {
  filterRoutes(store, menus) {
    // 1、权限标识符数据集合menus
    // 2、动态路由的集合
    // 筛选原理：当动态路由中的name属性的值，
    // 存在在menus数组中的时候，说明该路由是具有访问权限的路由。
    const newRoutes = asyncRoutes.filter(item => {
      return menus.includes(item.name)
    })
    // newRoutes 即为筛选出来具有访问权限的路由
    store.commit('setRoutes', newRoutes)
    return newRoutes
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
