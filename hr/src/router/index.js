import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true // 自定义的属性，用于控制该路由是否显示在侧边栏导航上，true表示不显示
  },

  {
    path: '/', // 配置访问路径的书写
    component: Layout, // 注册对应的组件
    redirect: '/dashboard', // 重定向 --- 强制跳转到/dashboard这个路径上
    name: 'dashboard', // 路由名称
    children: [{ // 子路由
      path: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { // 路由元信息 --- 作用：就是用来保存数据的
        title: '首页', // 导航标题
        icon: 'dashboard' // 标题旁边的图标
      }
    }]
  },

  {
    path: '/test',
    component: () => import('@/views/test/index.vue')
  }
  // 404 page must be placed at the end !!!
  // { path: '*', redirect: '/404', hidden: true }
]

// 引入动态路由
// 引入多个模块的规则
import approvalsRouter from './modules/approvals'
import departmentsRouter from './modules/departments'
import employeesRouter from './modules/employees'
import permissionRouter from './modules/permission'
import attendancesRouter from './modules/attendances'
import salarysRouter from './modules/salarys'
import settingRouter from './modules/setting'
import socialRouter from './modules/social'

// 动态路由
export const asyncRoutes = [
  approvalsRouter,
  departmentsRouter,
  employeesRouter,
  permissionRouter,
  attendancesRouter,
  salarysRouter,
  settingRouter,
  socialRouter
]

const createRouter = () => new Router({
  mode: 'history', // 注意：设置了history后端必须要进行相应的配置,vue文档中有配置代码示例
  scrollBehavior: () => ({ y: 0 }), // 表示跳转路由的时候竖向滚动条回到顶部
  // routes: [...constantRoutes, ...asyncRoutes]
  base: '/admin/', // 设置路径的前缀(配置项目的基础地址)
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
// 重置路由的方法，
// 注意：这个方法不是官方提供的，是以前的一群大佬发现这个问题，讨论出来的解决方法，
// 详情请看：https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
