import Layout from '@/layout'
export default {
  path: '/employees',
  component: Layout,
  name: 'employees',
  children: [
    {
      path: '',
      component: () => import('@/views/employees/index.vue'),
      meta: {
        title: '员工',
        icon: 'people'
      }
    }, {
      path: 'import',
      component: () => import('@/views/import/index.vue'),
      hidden: true // 表示该路由不需要显示在侧边栏导航中
    }, {
      // 动态路由，:id表示id是一个参数，也就当我们跳转到详情页面的时候，
      // 必须要传递一个参数
      // 才能访问到该页面，像配置成这种形式的路由，我们就叫他动态路由
      // 语法：path: '路径/:参数名'
      path: 'detail/:id',
      component: () => import('@/views/employees/detail.vue'),
      hidden: true // 表示该路由不需要显示在侧边栏导航中
    }
  ]
}
