import Layout from '@/layout'
export default {
  path: '/settings',
  component: Layout,
  name: 'settings',
  children: [
    {
      path: '',
      component: () => import('@/views/setting/index.vue'),
      meta: {
        title: '设置',
        icon: 'setting'
      }
    }
  ]
}
